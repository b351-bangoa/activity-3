<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Activity 3</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
    <?php
    $person = new Person('Senku', '', 'Ishigami');
    $developer = new Developer('John', 'Finch', 'Smith');
    $engineer = new Engineer('Harold', 'Myers', 'Reese');
    ?>

    <h1>Person</h1>
    <?= $person->printName(); ?>

    <h1>Developer</h1>
    <?= $developer->printName(); ?>

    <h1>Engineer</h1>
    <?= $engineer->printName(); ?>
    </body>
</html>